﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_5;

namespace Task_5_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Polinom a = new Polinom(new double[] { 2, 5, -4, 0, 1, 4 });
            Polinom b = new Polinom(new double[] { 2, 5, -4, 0, 1, 4 });
            Assert.AreEqual("4x^5+10x^4-8x^3+2x+8", (a+b).ToString());
        }

        [TestMethod]
        public void TestMethod2()
        {
            Polinom a = new Polinom(new double[] {0});
            Assert.AreEqual("0", a.ToString());
        }

        [TestMethod]
        public void TestMethod3()
        {
            Vector a = new Vector(new double[] { 1,2,4,6 });
            Assert.AreEqual(" 1 2 4 6", a.ToString());
        }

        [TestMethod]
        public void TestMethod4()
        {
            Vector a = new Vector(new double[] { 0, 4, 1, 2 });
            Vector b = new Vector(new double[] { 1, 2, -2, 0 });
            Assert.AreEqual(6, a*b);
        }
    }
}
