﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_5
{
    public class Polinom
    {

        private double[] koef;
        public Polinom(double[] arr)
        {
            if (arr == null) throw new Exception("Ошибка, массив пуст");
            koef = new double[arr.Length];
            Array.Copy(arr,koef,koef.Length);
        }
        public Polinom()
        {
        }
        public Polinom(int deg)
        {
            if (deg<=0) throw new Exception("Нет смысла создать нулевой многочлен");
            koef = new double[deg];
        }
        public double this[int index]
        {
            get
            {
                if (index < 0) throw new Exception("Недопустимый индекс элемента");
                return koef[index];
            }
            set
            {
                if (index<0) throw new Exception("Недопустимый индекс элемента");
                koef[index] = value;
            }
        }
        public int Length()
        {
            return koef.Length;
        }
        public static Polinom operator +(Polinom A, Polinom B)
        {
            if (A == null || B == null) throw new Exception("Ошибка, пустые ссылки");
            
            int a = A.Length();
            int b = B.Length();
            int c = a;
            if (a > b) c = b;

            
            double[] temp = new double[Math.Abs(b-a)+c];

            for (int i = 0; i < c; i++)
            {
                temp[Math.Abs(b - a) + c - i - 1] = A.koef[a - i - 1] + B.koef[b - i - 1];
            }

            if (b > a)
                for (int i = 0; i < Math.Abs(b - a); i++)
                {
                    temp[i] = B.koef[i];
                }
            else
            {
                for (int i = 0; i < Math.Abs(b - a); i++)
                {
                    temp[i] = A.koef[i];
                }
            }

            return new Polinom(temp);
        }
        public static Polinom operator -(Polinom A, Polinom B)
        {
            if (A == null || B == null) throw new Exception("Ошибка, пустые ссылки");
            int a = A.Length();
            int b = B.Length();
            int c = a;
            if (a > b) c = b;


            double[] temp = new double[Math.Abs(b - a) + c];

            for (int i = 0; i < c; i++)
            {
                temp[Math.Abs(b - a) + c - i - 1] = A.koef[a - i - 1] - B.koef[b - i - 1];
            }

            if (b > a)
                for (int i = 0; i < Math.Abs(b - a); i++)
                {
                    temp[i] = -B.koef[i];
                }
            else
            {
                for (int i = 0; i < Math.Abs(b - a); i++)
                {
                    temp[i] = A.koef[i];
                }
            }

            return new Polinom(temp);
        }
        public static bool operator ==(Polinom A, Polinom B)
        {
            if (A == null && B == null) throw new Exception("Ошибка, пустые ссылки");
            if ((A == null && B != null) || (B == null && A != null)) return false;
            if (A.Length() != B.Length())
            {
                return false;
            }
            else
            {
                for (int i = 0; i < A.Length(); i++)
                {
                    if (A.koef[i] != B.koef[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public static bool operator !=(Polinom A, Polinom B)
        {
            if (A==B)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public static bool operator <(Polinom A, Polinom B)
            {
                if (A == null || B == null) throw new Exception("Ошибка, пустые ссылки");
                if (A.Length() < B.Length())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        public static bool operator >(Polinom A, Polinom B)
        {
            if (A == null || B == null) throw new Exception("Ошибка, пустые ссылки");
            if (A.Length() > B.Length())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public override string ToString()
        {
            string a = "";
            if (koef[0] == 0 && koef.Length == 1)
            {
                return "0";
            }
            for (int i = 0; i < koef.Length-2; i++)
            {
                if (i != 0)
                {
                    if (koef[i] > 0)
                    {
                        a += "+";
                    }
                    else
                    {
                        if (koef[i] == 0)
                        {
                            continue;
                        }
                    }
                }

                    a += koef[i].ToString() + "x^" + (koef.Length - i - 1);
            }
            if (koef[koef.Length - 2] > 0)
            {
                a += "+";
            }
            a += koef[koef.Length-2].ToString()+"x";
            if (koef[koef.Length - 1] > 0)
            {
                a += "+";
            }
            a += koef[koef.Length - 1].ToString();
            return a;
        }
        public override int GetHashCode()
        {
            int code = 0;
            for (int i = 0; i < koef.Length; i++)
            {
                code += 10 * i;
            }
            return code;
        }
        public override bool Equals(object o)
        {
            if (o == null) throw new Exception("Пустая ссылка");
            if (o is Polinom)
            {
                Polinom A = (Polinom)o;
                return A == this;
            }
            else
            {
                throw new Exception("Недопустимый тип");
            }
        }
    }

    public class Vector
    {
        private double[] koef;
        public double this[int index]
        {
            get
            {
                if (index < 0) throw new Exception("Недопустимый индекс элемента");
                return koef[index];
            }
            set
            {
                if (index < 0) throw new Exception("Индекс не может быть меньше нуля");
                koef[index] = value;
            }
        }
        public Vector (double[] arr)
        {
            if (arr == null) throw new Exception("Пустая ссылка на массив");
            koef = new double[arr.Length];
            Array.Copy(arr,koef,koef.Length);
        }
        public Vector()
        {
        }
        public Vector(int deg)
        {
            if (deg <= 0) throw new Exception("Странная недопустимая размерность");
            koef = new double[deg];
        }
        public int Length()
        {
            return koef.Length;
        }
        public static Vector operator +(Vector A, Vector B)
        {
            if (A == null || B == null) throw new Exception("Невозможно сложение пустых ссылок");
            if(A.Length()!=B.Length())
            {
                throw new Exception("Не совпадают размеры векторов");
            }else
            {
                double[] k = new double[A.Length()];
                    for (int i = 0; i < A.Length(); i++)
                    {
                        k[i] = A.koef[i] + B.koef[i];
                    }
                return new Vector(k);
            }
        }
        public static Vector operator -(Vector A, Vector B)
        {
            if (A == null || B == null) throw new Exception("Невозможно найти разность пустых ссылок");
            if (A.Length() != B.Length())
            {
                throw new Exception("Не совпадают размеры векторов");
            }
            else
            {
                double[] k = new double[A.Length()];
                for (int i = 0; i < A.Length(); i++)
                {
                    k[i] = A.koef[i] - B.koef[i];
                }
                return new Vector(k);
            }
        }
        public static double operator *(Vector A, Vector B)
        {
            if (A == null || B == null) throw new Exception("Невозможно произведение пустых ссылок");
            if (A.Length() != B.Length())
            {
                throw new Exception("Не совпадают размеры векторов");
            }
            else
            {
                double k = 0;
                for (int i = 0; i < A.Length(); i++)
                {
                    k += A.koef[i] * B.koef[i];
                }
                return k;
            }
        }
        public double Lengthd()
        {
            double sum = 0;
            for (int i = 0; i < koef.Length; i++)
            {
                sum += koef[i] * koef[i];
            }
            return Math.Sqrt(sum);
        }
        public static double Angle(Vector A, Vector B)
        {
            if (A == null || B == null) throw new Exception("Невозможно сложение пустых ссылок");
            if (A.Length() != B.Length())
            {
                throw new Exception("Не совпадают размеры векторов");
            }
            else
            {

                return Math.Acos(A * B / A.Lengthd() / A.Lengthd());
            }
        }
        public override string ToString()
        {
            string a = "";
            for (int i = 0; i < koef.Length; i++)
            {
                a += " " + koef[i].ToString();
            }
            return a;
        }
        public override int GetHashCode()
        {
            int code = 0;
            for (int i = 0; i < koef.Length; i++)
            {
                code += 10 * i;
            }
            return code;
        }
        public override bool Equals(object o)
        {
            if (o == null) throw new Exception("Пустая ссылка");
            if (o is Vector)
            {
                Vector A = (Vector)o;
                return A == this;
            }
            else
            {
                throw new Exception("Несовместимость типов");
            }
        }
    }
}
