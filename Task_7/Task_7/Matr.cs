﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_7
{
    public class MatrixException : Exception
    {
        public MatrixException(string mes)
            : base(mes)
        {
        }
        public MatrixException()
            :base("Попытка произведения действий")
        {
        }
    }

    public class Matr: IComparable
    {
        public int CompareTo(object a)
        {
            if (!(a is Matr)) throw new Exception("Несовместимость типов");
            if ( ((Matr)a).MX.Length > MX.Length)
            {
                return -1;
            }
            else
            {
                if (((Matr)a).MX.Length < MX.Length)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        private double[,] MX;
        public Matr(int n, int m)
        {
            if (n > 0 && m > 0)
            {
                MX = new double[m, n];
            }
            else
            {
                throw new Exception("Какой-то подозрительный размер матрицы, не нравится он мне");
            }
        }
        public Matr(double[,] arr)
        {
            if (arr == null) throw new Exception("Пустая ссылка");
            MX = new double[arr.GetLength(0), arr.GetLength(1)];
            Array.Copy(arr, MX, arr.Length);
        }

        public static Matr operator +(Matr A, Matr B)
        {
            if (A == null || B == null) throw new Exception("Пустые ссылки");
            if (A.MX.GetLength(0) == B.MX.GetLength(0) && A.MX.GetLength(1) == B.MX.GetLength(1))
            {
                double[,] c = new double[A.MX.GetLength(0), A.MX.GetLength(1)];
                for (int i = 0; i < A.MX.GetLength(0); i++)
                {
                    for (int j = 0; j < A.MX.GetLength(1); j++)
                    {
                        c[i, j] = A.MX[i, j] + B.MX[i, j];
                    }
                }
                return new Matr(c);
            }
            else
            {
                throw new MatrixException("Матрицы несовместимы для суммы");
            }
        }
        public static Matr operator -(Matr A, Matr B)
        {
            if (A == null || B == null) throw new Exception("Пустые ссылки");
            if (A.MX.GetLength(0) == B.MX.GetLength(0) && A.MX.GetLength(1) == B.MX.GetLength(1))
            {
                double[,] c = new double[A.MX.GetLength(0), A.MX.GetLength(1)];
                for (int i = 0; i < A.MX.GetLength(0); i++)
                {
                    for (int j = 0; j < A.MX.GetLength(1); j++)
                    {
                        c[i, j] = A.MX[i, j] - B.MX[i, j];
                    }
                }
                return new Matr(c);
            }
            else
            {
                throw new MatrixException("Матрицы несовместимы для суммы");
            }
        }
        public string ToString()
        {      
            string str = "";
            for (int i = 0; i < MX.GetLength(0); i++)
            {
                for (int j = 0; j < MX.GetLength(1); j++)
                {
                    str += " " + MX[i, j].ToString();
                }
                str += '\n';
            }
            return str;
        }
        public static Matr operator *(Matr A, Matr B)
        {
            if (A == null || B == null) throw new Exception("Пустые ссылки");
            if (A.MX.GetLength(1) == B.MX.GetLength(0))
            {
                double[,] c = new double[A.MX.GetLength(0), B.MX.GetLength(1)];

                for (int i = 0; i < A.MX.GetLength(0); i++)
                {
                    for (int j = 0; j < B.MX.GetLength(1); j++)
                    {
                        for (int k = 0; k < B.MX.GetLength(0); k++)
                        {
                            c[i, j] += A.MX[i, k] * B.MX[k, j];
                        }
                        
                    }
                }
                return new Matr(c);
            }
            else
            {
                throw new MatrixException("Матрицы несовместимы для произведения");
            }
        }
        public double this[int index1, int index2]
        {
            get
            {
                if (index1 < 0 || index2 < 0) throw new Exception("Недопустимые индексы");
                return MX[index1, index2];
            }
            set
            {
                if (index1 < 0 || index2 < 0) throw new Exception("Недопустимые индексы");
                MX[index1, index2] = value;
            }
        }

    }
}
