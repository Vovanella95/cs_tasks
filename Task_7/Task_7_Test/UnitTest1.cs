﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_7;

namespace Task_7_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            double[,] a = {{1,0,0},{0,1,0},{0,0,1}};
            double[,] b = {{5},{6},{1}};
            Matr A = new Matr(a);
            Matr B = new Matr(b);
            Assert.AreEqual((A * B).CompareTo(B), 0);
        }

        [TestMethod]
        public void TestMethod2()
        {
            double[,] a = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
            Matr A = new Matr(a);
            Assert.AreEqual(" 2 0 0\n 0 2 0\n 0 0 2\n", (A + A).Test());
        }

        [TestMethod]
        public void TestMethod3()
        {
            double[,] a = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
            double[,] b = { { 5 }, { 6 }, { 1 } };
            Matr A = new Matr(a);
            Matr B = new Matr(b);
            Assert.AreEqual(A.CompareTo(B), 1);
        }
    }
}
