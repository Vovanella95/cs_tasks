﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task_7;

namespace Task_7_Wow
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            double[,] a = { { 3, 6, 2 }, { 4, 3, 7 }, { 4, 3, 7 } };
            double[,] b = { {1,5},{ 2,6}, {3,9}};
            //double[,] b = { {1,5},{ 2,6}};
            Matr A = new Matr(a);
            Matr B = new Matr(b);
            
            try
            {
                label1.Content = (A * B).Test();
                //label1.Content = B.CompareTo(B).ToString();
            }
            catch (MatrixException ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
