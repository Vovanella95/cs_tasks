﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace Task_9
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openfile = new OpenFileDialog();
            openfile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openfile.ShowDialog();
            string way = openfile.FileName;
            StreamReader reader = new StreamReader(way);
            string text = reader.ReadToEnd();
            OutBox.Text = text;
            reader.Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SaveFileDialog savefile = new SaveFileDialog();
            savefile.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            savefile.ShowDialog();
            StreamWriter writer = new StreamWriter(savefile.FileName);
            writer.Write(OutBox.Text);
            writer.Close();
        }
    }
}
