﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Task_2;

namespace Task_2_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                double A = Convert.ToDouble(NumberInput.Text);
                int n = Convert.ToInt32(DegInput.Text);
                if (AccuracyInput.Text != "")
                {
                    double eps = Convert.ToDouble(AccuracyInput.Text);
                    OutputAnswer.Content = "= " + Convert.ToString(Task_2_Realisation.NewtonSqr(A, n, eps));
                    MathPower.Content = "Math.Pow(A,n) = " + Math.Pow(A, 1.0 / n);
                }
                else
                {
                    OutputAnswer.Content = "= " + Convert.ToString(Task_2_Realisation.NewtonSqr(A, n));
                    MathPower.Content = "Math.Pow(A,n) = " + Math.Pow(A, 1.0 / n);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ОШИБКА ВВОДА\n" + ex.Message);
            }
        }
    }
}
