﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_2
{
    
    /// <summary>
    /// This is a class, each can calculate sqr of number and transform number to binary form
    /// </summary>
    public class Task_2_Realisation
    {
        /// <summary>
        /// Calculating sqr where A=0
        /// </summary>
        /// <param name="A">element A</param>
        /// <param name="n">degree of sqr</param>
        /// <param name="epsilon">accyracy</param>
        /// <returns></returns>
        public static double Newton(double A, int n, double epsilon = 0.001)
        {
            double x0 = A;
            double x1 = 1.0 / n * ((n - 1.0) * x0 + (A / Math.Pow(x0, n - 1.0)));
            while (Math.Abs(x1 - x0) > epsilon)
            {
                x0 = x1;
                x1 = 1.0 / n * ((n - 1.0) * x0 + (A / Math.Pow(x0, n - 1.0)));
            }

            return x1;
        }

        /// <summary>
        /// Find the sqr root n degree from x
        /// </summary>
        /// <param name="A">argument</param>
        /// <param name="n">sqr degree</param>
        /// <param name="epsilon">the accuracy of calculation</param>
        /// <returns></returns>
        public static double NewtonSqr(double A, int n, double epsilon=0.001)  
        {
            if (A == 0) return 0;

            if (A > 0) 
            {
                if (n >= 0)
                {
                    return Newton(A, n, epsilon);
                }
                else
                {
                    return 1 / Newton(A, -n, epsilon);
                }
            }
            else
            {
                if (n % 2 == 0)
                {
                    throw new Exception("Отрицательное число под корнем четной степени!");
                }
                else
                {
                    if (n > 0)
                    {
                        return -Newton(-A, n, epsilon);
                    }
                    else
                    {
                        return -1/Newton(-A, -n, epsilon);
                    }
                }
            }
        }

        /// <summary>
        /// Converting the number into binary form and return in as a string
        /// </summary>
        /// <param name="num">number, each must been transformed</param>
        /// <returns></returns>
        public static string NumberConverter(int num)
        {
            if (num < 0) throw new Exception("Введите неотрицательное число");
            if (num == 0) return "0";
            StringBuilder DoubleNum = new StringBuilder("");
            while (num != 0)
            {
                DoubleNum.Insert(0,(num % 2).ToString());
                num /= 2;
            }
            return DoubleNum.ToString();
        }
    }
}
