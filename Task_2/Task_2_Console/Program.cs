﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_2;

namespace Task_2_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Task_2_Realisation a = new Task_2_Realisation();
            Console.WriteLine("Welcome to this program! Please, enter the A parametr");
            double A = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("All right, and now enter the degree of sqr");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("All right, and now enter the accyracy of calculating");
            double epsilon = Convert.ToDouble(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("\n\nOkay, I trying to solve it \n\n\nYour solution:     " + a.NewtonSqr(A, n, epsilon) + "\nMath.Pow solution: " + Math.Pow(A, 1.0 / n));
            Console.ReadLine();
        }
    }
}
