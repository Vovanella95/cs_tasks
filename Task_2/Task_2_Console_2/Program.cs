﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task_2;

namespace Task_2_Console_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("<EPAM>_Task_2\n\nВведите число под корнем");
            double A = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите степень корня");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите epsilon или нажмите Enter для системной точности");
            string c = Console.ReadLine();
            if (c != "")
            {
                try
                {
                    double eps = Convert.ToDouble(c);
                    Console.WriteLine(new Task_2_Realisation().NewtonSqr(A, n, eps));
                }
                catch(Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
            }
            else
            {
                try
                {
                    Console.WriteLine(new Task_2_Realisation().NewtonSqr(A, n));
                }
                catch(Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                }
            }
            Console.ReadLine();
        }
    }
}
