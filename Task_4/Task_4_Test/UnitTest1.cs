﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_4;

namespace Task_4_Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Triangle a = new Triangle(5, 8, 5);
            Assert.AreEqual(18, a.Perimetr());
        }

        [TestMethod]
        public void TestMethod2()
        {
            Triangle a = new Triangle(3, 4, 5);
            Assert.AreEqual(6, a.Square());
        }

        [TestMethod]
        public void TestMethod3()
        {
            Triangle a = new Triangle(9, 4, 6);
            Assert.AreNotEqual(18, a.Square());
        }

    }
}
