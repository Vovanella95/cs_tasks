﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task_4;

namespace Task_1_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string[] nums = {""};
            double[] numbers = {0};
            try
            {
                nums = inputdata.Text.Split(' ');
                numbers = new double[nums.Length];
                for (int i = 0; i < nums.Length; i++)
                {
                    numbers[i] = Convert.ToDouble(nums[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            Triangle tr = new Triangle();
            if (numbers.Length == 3)
            {
                try
                {
                    tr = new Triangle(numbers[0], numbers[1], numbers[2]);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                if (numbers.Length == 6)
                {
                    try
                    {
                        tr = new Triangle(numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Проверьте правильность введенных данных");
                    return;
                }
                }

            pl.Content = "Площадь: "+tr.Square().ToString();
            per.Content = "Периметр: " + tr.Perimetr().ToString();
        }
    }
}
