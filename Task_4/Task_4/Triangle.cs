﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_4
{
    public class Triangle
    {
        private double a, b, c;

        /// <summary>
        /// Конструктор, создающий треугольник, если создать такой можно
        /// </summary>
        /// <param name="a">Длина первой стороны</param>
        /// <param name="b">Длина второй стороны</param>
        /// <param name="c">Длина третьей стороны</param>
        public Triangle(double a, double b, double c)
        {
            if (a != 0 && b != 0 && c != 0 && a + b > c && b + c > a && a + c > b)
            {
                this.a = a;
                this.b = b;
                this.c = c;
            }
            else throw new Exception("Невозможно создать треугольник");
        }

        /// <summary>
        /// Конструктор, создающий треугольник, если это возможно
        /// </summary>
        /// <param name="x1">x координата первой точки</param>
        /// <param name="y1">y координата первой точки</param>
        /// <param name="x2">x координата второйточки</param>
        /// <param name="y2">y координата второйточки</param>
        /// <param name="x3">x координата третей точки</param>
        /// <param name="y3">y координата третей точки</param>
        public Triangle(double x1, double y1, double x2, double y2, double x3, double y3)
            :this(Math.Sqrt(Math.Pow(Math.Abs(x2 - x1),2) + Math.Pow(Math.Abs(y2 - y1),2)),Math.Sqrt(Math.Abs(x3 - x1) * Math.Abs(x3 - x1) + Math.Abs(y3 - y1) * Math.Abs(y3 - y1)),Math.Sqrt(Math.Abs(x2 - x3) * Math.Abs(x2 - x3) + Math.Abs(y2 - y3) * Math.Abs(y2 - y3)))
        {
           
        }
       
        /// <summary>
        /// Обычный конструктор по умолчанию
        /// </summary>
        public Triangle()
        {
            a = 1;
            b = 1;
            c = 1;
        }

        /// <summary>
        /// Возвращает периметр треугольника
        /// </summary>
        /// <returns>Периметр треугольника</returns>
        public double Perimetr()
        {
            return a + b + c;
        }

        /// <summary>
        /// Возвращает площадь треугольника
        /// </summary>
        /// <returns>Площадь треугольника</returns>
        public double Square()
        {
            double p = Perimetr() / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
}
