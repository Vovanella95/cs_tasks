﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task_6;
using Microsoft.Win32;
using System.IO;

namespace Task_3_Demo
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
        }
        MyReader a;
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Scroll_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {
            double x = e.NewValue;

        }

        private void Scr_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {

        }

        private void ScrollBar_Scroll_1(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {
            double x = e.NewValue;
            MyReader reader = new MyReader(way);
            OutBox.Content = reader.ReadPart(x);
            
        }

        private string way;

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                Password pass = new Password();
                pass.ShowDialog();
                if (pass.GetPass() == Properties.Resources.Password)
                {
                    OpenFileDialog a = new OpenFileDialog();
                    a.ShowDialog();
                    way = a.FileName;
                    Slide.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("Пароль не правильный");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ProgramHelper helper = new ProgramHelper();
            if(helper.CodeCheckSyntax(InputText.Text,"VB"))
            {
                InputText.Text = helper.ConvertToCSharp(InputText.Text);
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            ProgramHelper helper = new ProgramHelper();
            if(helper.CodeCheckSyntax(InputText.Text,"C#"))
            {
                InputText.Text = helper.ConvertToVB(InputText.Text);
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            ProgramHelper helper = new ProgramHelper();
            if (helper.CodeCheckSyntax(InputText.Text, "C#"))
            {
                Answer.Content = "Yes";
            }
            else
            {
                Answer.Content = "No";
            }
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            ProgramHelper helper = new ProgramHelper();
            if (helper.CodeCheckSyntax(InputText.Text, "VB"))
            {
                Answer.Content = "Yes";
            }
            else
            {
                Answer.Content = "No";
            }
        }
    }
}
