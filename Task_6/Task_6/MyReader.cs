﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Task_6
{
    interface IConvertible
    {
        string ConvertToCSharp(string a);
        string ConvertToVB(string a);
    }


    interface ICodeChecker
    {
        bool CodeCheckSyntax(string str, string lang);
    }

    public class ProgramHelper : ProgramConverter, ICodeChecker
    {
        public bool CodeCheckSyntax(string str, string lang)
        {
            if (str == null || lang == null)
            {
                throw new Exception("Вы что-то не то ввели");
            }
            
            if (str.IndexOf("using") >= 0)
            {
                if (lang == "C#")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (lang == "C#")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            throw new Exception("Введенный язык некоректен");
        }
    }

    public class ProgramConverter : IConvertible
    {
        public string ConvertToVB(string a)
        {
            if (a == null)
            {
                throw new Exception("Вы что-то не то ввели");
            }
            a = a.Replace("using","USES");
            a = a.Replace("for","FORECH");
            a = a.Replace("int","INT");
            return a;
        }
        public string ConvertToCSharp(string a)
        {
            if (a == null)
            {
                throw new Exception("Вы что-то не то ввели");
            }
            a = a.Replace("USES", "using");
            a = a.Replace("FORECH", "for");
            a = a.Replace("INT", "int");
            return a;
        }
    }

    public class MyReader : StreamReader
    {
        public MyReader(string way)
            : base(way)
        {

        }

        public string ReadPart(double p)
        {
            long length = ReadToEnd().Length;
            BaseStream.Position = 0;
            string part = "";
            int kol = (int)(length * p);
            for (int i = 0; i < kol; i++)
            {
                part += ((char)Read()).ToString();
            }
            Close();
            return part;
            
        }

    }
    
}