﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Task_3;

namespace Task_3_WonForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Color eucolor = Color.Red, stcolor = Color.Blue;

            string[] numbers = textBox2.Text.Split(' ');

            if (numbers.Length < 2)
            {
                MessageBox.Show("Введено менее одного числа");
                return;
            }

            long[] nums = new long[numbers.Length];
            try
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    nums[i] = Convert.ToInt64(numbers[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            long time1;
            long time2;

            try
            {
                label1.Text = "Евклид: " + Task_3_Realisation.EuklidMethod(out time1, nums).ToString();
                label2.Text = "Стейн: " + Task_3_Realisation.SteinMethod(out time2, nums).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            



            if (checkBox1.Checked)
            {
                ColorDialog col = new ColorDialog();
                col.ShowDialog();
                eucolor = col.Color;
                col.ShowDialog();
                stcolor = col.Color;
            }

            if (checkBox2.Checked)
            {
                ShowDiagramm(time1, time2, eucolor, stcolor, true);
            }
            else
            {
                ShowDiagramm(time1, time2, eucolor, stcolor);
            }

        }

        private void ShowDiagramm(long time1, long time2, Color eucolor, Color stcolor, bool isvert=false)
        {
            string[] a = new string[] { "" };
            double[] b = new double[] { time1 };

            string[] c = new string[] { "" };
            long[] d = new long[] { time2 };
            chart1.Series["Euklid"].Points.DataBindXY(a, b);
            chart1.Series["Stein"].Points.DataBindXY(c, d);
            chart1.Series["Euklid"].Color = eucolor;
            chart1.Series["Stein"].Color = stcolor;

            if (isvert)
            {
                chart1.Series["Euklid"].ChartType = SeriesChartType.Bar;
                chart1.Series["Stein"].ChartType = SeriesChartType.Bar;
            }
        }

        private void panel1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            
        }
    }
}
