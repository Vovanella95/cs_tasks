﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_3;

namespace Task_3_Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            long time = 0;
            Assert.AreEqual(16,Task_3_Realisation.EuklidMethod(out time, 16, 32,64,128,256));
        }

        [TestMethod]
        public void TestMethod2()
        {
            long time = 0;
            Assert.AreNotEqual(3, Task_3_Realisation.EuklidMethod(out time, 4, 32, 64, 7, 256));
        }

        [TestMethod]
        public void TestMethod3()
        {
            long time = 0;
            Assert.AreEqual(12, Task_3_Realisation.EuklidMethod(out time, 0,12));
        }

        [TestMethod]
        public void TestMethod4()
        {
            long time = 0;
            Assert.AreEqual(649, Task_3_Realisation.SteinMethod(out time, 649, 0));
        }

        [TestMethod]
        public void TestMethod5()
        {
            long time = 0;
            Assert.AreEqual(0, Task_3_Realisation.SteinMethod(out time, 0, 0));
        }
    }
}
