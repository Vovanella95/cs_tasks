﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Task_3
{
    public class Task_3_Realisation
    {
        /// <summary>
        /// Метод, вычисляющий НОД от двух положительных чисел методом Евклида
        /// </summary>
        /// <param name="first">Первое число</param>
        /// <param name="second">Второе число</param>
        /// <returns></returns>
        private static long Euklid(long first, long second)
        {
            if (first < 0 || second < 0) throw new Exception("Введены некоррентные данные, возможно одно из чисел отрицательное");
            if (first < second)
            {
                first = first+second;
                second = first - second;
                first = first - second;
            }
            if (second == 0) return first;
            if (first % second == 0)
            {
                return second;
            }
            else
            {
                return Euklid(second, first % second);
            }
        }

        /// <summary>
        /// Метод, позволяющий вычислить НОД двух неотрицательных чисел медотом Стейна
        /// </summary>
        /// <param name="first">Первое число</param>
        /// <param name="second">Второе число</param>
        /// <returns></returns>
        private static long Stein(long first, long second)
        {
            if (first == second) return first;
            if (first == 0) return second;
            if (second == 0) return first;

            if (first % 2 == 0 && second % 2 == 0) return 2 * Stein(first / 2, second / 2);
            if (first % 2 == 0) return Stein(first / 2, second);
            if (second % 2 == 0) return Stein(first, second / 2);
            if (second > first)
            {
                return Stein(first, (second - first) / 2);
            }
            else
            {
                return Stein((first - second) / 2, second);
            }
        }

        /// <summary>
        /// Метод, позволяющий вычислить НОД произвольного числа чисел методом Стейна
        /// </summary>
        /// <param name="time">Выходной параметр время выполнения</param>
        /// <param name="elems">Числа</param>
        /// <returns></returns>
        public static long SteinMethod(out long time, params long[] elems)
        {

            Stopwatch a = new Stopwatch();
            a.Start();
            long temp = elems[0];
            for (int i = 1; i < elems.Length; i++)
            {
                temp = Stein(temp, elems[i]);
            }
            a.Stop();
            time = a.ElapsedTicks;
            return temp;
        }

        /// <summary>
        /// Метод, позволяющий вычилсить НОД произвольного числа чисел методом Стейна
        /// </summary>
        /// <param name="time">Возвращаемый параметр время выполнения</param>
        /// <param name="elems">Числа</param>
        /// <returns></returns>
        public static long EuklidMethod(out long time, params long[] elems)
        {
            Stopwatch a = new Stopwatch();
            a.Start();

            long temp = elems[0];
            for (int i = 1; i < elems.Length; i++)
            {
                temp = Euklid(temp, elems[i]);
            }

            a.Stop();
            time = a.ElapsedTicks;

            return temp;
        }
    }
}
