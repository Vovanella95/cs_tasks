﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Task_1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Tasl_1_Realise cl = new Tasl_1_Realise();
            Assert.AreEqual("X: 12,22 Y: 21,11\n", cl.ReadFromString("12.22,21.11"));
        }
        [TestMethod]
        public void TestMethod2()
        {
            Tasl_1_Realise cl = new Tasl_1_Realise();
            Assert.AreNotEqual("X: 35,15 Y: 53,20\n", cl.ReadFromString("35.15,53.21"));
        }
        [TestMethod]
        public void TestMethod3()
        {
            Tasl_1_Realise cl = new Tasl_1_Realise();
            Assert.AreEqual("X: 12,3413 Y: 31,1234\n", cl.ReadFromString("12.3414,31.1234"));
        }
    }
}
