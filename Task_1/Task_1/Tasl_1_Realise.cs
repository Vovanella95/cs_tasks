﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Task_1
{
    /// <summary>
    /// Class to convert the input data to normal form
    /// </summary>
    public class Tasl_1_Realise
    {
        /// <summary>
        /// Taking text from file and transform it to normal form according to example
        /// </summary>
        /// <param name="wayToFile">The Way to File, from each we will take the text</param>
        /// <returns></returns>
        public string ReadFromFile(string wayToFile)
        {
            StreamReader reader = new StreamReader(wayToFile);
            return ReadFromString(reader.ReadToEnd()); 
        }
        /// <summary>
        /// Taking text and transform it to normal form according to example
        /// </summary>
        /// <param name="stringLines">The text, each must be transform</param>
        /// <returns></returns>
        public string ReadFromString(string stringLines)
        {
                string []lines = stringLines.Split('\n');
                StringBuilder a = new StringBuilder("");
                string[] linesMini;
                for (int i = 0; i < lines.Length; i++)
			{
                    //Regex cheking
                Regex CheckLine = new Regex(@"[\+-]?\d+(.\d+)?,[\+-]?\d+(.\d+)?");
                Match IsError = CheckLine.Match(lines[i]);
                if (!IsError.Success) throw new Exception("Ошибка в формате входных данных");

                    //Replacing substrings
                
                linesMini = lines[i].Split(',');
                a.Append(String.Format("X: {0} Y: {1}", linesMini[0].Replace(".", ","), linesMini[1].Replace(".", ",") + "\n"));
            }
                return a.ToString();
        }
    }
}
